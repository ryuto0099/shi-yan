package com.example.demo.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.PullDownTestData;
import com.example.demo.repository.PullDownTestDataRepository;

@Service
public class TestService {
	@Autowired
	private PullDownTestDataRepository pullDownTestDataRepository;
	
	public List<PullDownTestData> getAllTagDatas() {
		List<PullDownTestData> list = pullDownTestDataRepository.findAll();
		return list;
	}
	
	public List<String> getAllTagDatasString() {
		List<String> list = new ArrayList<>();
		for(PullDownTestData data : pullDownTestDataRepository.findAll()) {
			list.add(data.getTagData());
		}
		
		return list;
	}
}
