package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.demo.entity.PullDownTestData;
import com.example.demo.service.TestService;

@Controller
@ResponseBody
public class OptionsContoller {
	@Autowired
	private TestService testService;
	
	@GetMapping("/api/json")
	private ResponseEntity<List<String>> json() {
		List<String> data = testService.getAllTagDatasString();
		return ResponseEntity.ok(data);
	}
	
	@GetMapping("/api/json2")
	private List<PullDownTestData> json2() {
		List<PullDownTestData> list = testService.getAllTagDatas();
		return list;
	}
}
