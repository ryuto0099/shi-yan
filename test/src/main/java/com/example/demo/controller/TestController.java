package com.example.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class TestController {	
	@GetMapping("/vue")
	private String test2() {
		return "test2.html";
	}
	
	@RequestMapping(value= "/api/post", method = RequestMethod.POST)
	private String test3(@RequestParam("tag") String selected, Model model) {
		model.addAttribute("selected", selected);
		return "test3.html";
	}
}
